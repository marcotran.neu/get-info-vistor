from typing import Optional
from sqlmodel import Field, Session, SQLModel, create_engine, select


class Visitor(SQLModel, table=True):
    visitor_id: Optional[int] = Field(default=None, primary_key=True)
    visitor_first_name: str
    visitor_last_name: str
    visitor_email: str
    visitor_code: str
    visitor_phone: Optional[int]


class VisitorCreate(Visitor):
    pass
