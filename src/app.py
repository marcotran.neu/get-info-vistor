from fastapi import FastAPI, status
from fastapi.middleware.cors import CORSMiddleware
from connection.database import engine
from models.visitor import *
from sqlmodel import Session
import os

app = FastAPI()
origins = [
    "http://localhost",
    "http://localhost:8080",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
session = Session(bind=engine)


@app.post('/visitor', response_model=Visitor,
          status_code=status.HTTP_201_CREATED)
async def create_a_book(visitor: Visitor):
    new_visitor = Visitor(visitor_first_name=visitor.visitor_first_name,
                          visitor_last_name=visitor.visitor_last_name,
                          visitor_email=visitor.visitor_email,
                          visitor_code=visitor.visitor_code,
                          visitor_phone=visitor.visitor_phone)

    session.add(new_visitor)

    session.commit()

    return new_visitor


