from sqlmodel import create_engine, SQLModel, Session

engine = create_engine("postgresql://postgres:changeme@52.77.215.229:5432/visitor")


def get_session():
    with Session(engine) as session:
        yield session
